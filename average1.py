def main():
  n = eval(input("How many numbers are there? "))

  sum = 0.0
  for i in range(n):
    num = eval(input("Enter a number >> "))
    sum = sum + num

  print("\nThe average of the numbers is", sum / n)

main()