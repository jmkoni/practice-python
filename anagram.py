import time

# finds number of words that are anagrams of any other word in the list

# anagram_count2 was my inital effort. It didn't sort the list as a whole
# and compared each sorted string to every other string.
# It took 40 seconds to sort a list of 50000 words. Too slow.
def anagram_count2(array):
	alpha = []
	count = 0
	for i in array:
		alpha.append(''.join(sorted(i)))
	for i in alpha:
		if alpha.count(i) > 1:
			count = count + 1
			print count
	return count

# anagram_count is the final function. It sorts the string, then sorts the list.
# Then I compare each string to the string before and after it. If it matches either,
# I add one to the count. It take 4.5 sec for a 1M list and 18.7 for a 3.5M+ list.
def anagram_count(array):
	alpha = []
	count = 0
	for i in array:
		alpha.append(''.join(sorted(i)))
	alpha.sort()
	for i in range(len(alpha)):
		if i == 0:
			if alpha[i] == alpha[i+1]:
				count = count + 1
		if i == (len(alpha) - 1):
			if alpha[i] == alpha[i-1]:
				count = count + 1
		else:
			if (alpha[i] == alpha[i+1]) | (alpha[i] == alpha[i-1]):
				count = count + 1
	return count

# generates a word array from a list of nouns, to test anagram_count on a larger scale.
def word_array(txtfile):
	logfile = open(txtfile, "r").readlines()
	wordlist = []
	for line in logfile:
		for word in line.split():
			wordlist.append(word)
	return wordlist

start_time = time.time()
print "\noriginal count:"
print anagram_count( ['act', 'cat', 'dog', 'dog', 'aardvark'] )
print "\ncount from hugelist (length: " + str(len(word_array("hugelist.txt"))) + "):"
print anagram_count(word_array("hugelist.txt"))

print time.time() - start_time, "seconds"