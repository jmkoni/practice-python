from collections import defaultdict

class WordCounter(object):
	def __init__(self):
		self.words = defaultdict(int)
#removed for defaulttdict
#		self.words = []

#	def add(self, word):
#		self.words.append(word)

#	def get_count(self, word):
#		count = 0
#		for w in self.words:
#			if w == word:
#				count += 1
#		return count

	def add(self, word):
		self.words[word]+=1
#not used for default dict
#		if word not in self.words:
#			self.words[word]=0
#		self.words[word] +=1
#	def get_count(self, word):
#		return self.words.get(word, 0)

wc = WordCounter()
for word in 'this is a sentence'.split(' '):
	wc.add(word)
print(wc.words)
