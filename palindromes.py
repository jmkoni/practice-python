def compute_palindromes(words):
	# Write your code here
	# To print results to the standard output you can use print
	# Example print "Hello world!"
	for i in words:
		if len(i) < 2:
			print i
		elif i == i[::-1]:
			print i
		else:
			startstring = ""
			endstring = ""
			middle = ""
			remainingletters = ""
			for l in i:
				if i.count(l) == 2:
					if startstring.find(l) == -1:
						startstring = l + startstring
						endstring = endstring + l
				elif i.count(l) == 1:
					middle = middle + l
				else:
					remainingletters = remainingletters + l
			if len(middle) > 1:
				print -1
			elif len(remainingletters) > 0:
				print -1
			else:
				print startstring + middle + endstring
					