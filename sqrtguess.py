import math

def main():
  print("Guesses the square root of a number a given number of times.")
  x = eval(input("Please enter the number you would like the square root of: "))
  numguess = eval(input("Please enter the number of times you would like to guess: "))
  if x < 1:
    print("Please enter a number greater than zero.")
    return
  if not isinstance(x, int):
    print("Please enter an integer")
    return
  guess = x / 2
  for i in range(numguess):
    guess = (guess + (x / guess)) / 2

  diff = math.sqrt(x) - guess
  print("The final guess for the square root of", x, "is", guess)
  print("The actual square root is", math.sqrt(x),"which is a difference of", diff)

main()
