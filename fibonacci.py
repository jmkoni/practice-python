def main():
  print("Computer Fibonacci up to the nth number")
  n = eval(input("Please enter the place of the Fibonacci number that you would like: "))
  f1 = 0
  f2 = 1
  for i in range(n-1):
    f = f1 + f2
    f1 = f2
    f2 = f
  print("The", n, "th value of the Fibonacci sequence is", f)

main()
