from collections import defaultdict
bigram_dict = defaultdict(lambda: defaultdict(int))
# count bigrams in text
bigram_count = defaultdict(lambda: defaultdict(int))
bigrams = [("baa","baa"),("baa","black"),("black","sheep")]
for word1, word2 in bigrams:
	bigram_count[word1][word2] += 1
print bigram_count