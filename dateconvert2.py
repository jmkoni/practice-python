def main():
  print("This program converts strings to dates")
  day, month, year = eval(input("Please enter the day, month, year numbers: "))

  date1 = str(month) + "/" + str(day) + "/" + str(year)

  monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "October", "November", "December"]
  monthStr = monthNames[int(month)-1]

  date2 = monthStr + " " + str(day) + ", " + str(year)

  print("The date is {0}/{1}/{2} or {3} {1}, {2}.".format(month,day,year,monthStr))

main()