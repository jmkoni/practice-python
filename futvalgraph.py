from graphics import *
def main():
  print("This program graphs out the future value of your investment.")
  principal = eval(input("Please enter the principal balance: "))
  rate = eval(input("Please enter the yearly interest rate: "))

  # Create the graphics window
  win = GraphWin("Investment Growth Chart", 320, 240)
  win.setBackground("white")
  Text(Point(20, 230), ' 0.0K').draw(win)
  Text(Point(20, 180), ' 2.5K').draw(win)
  Text(Point(20, 130), ' 5.0K').draw(win)
  Text(Point(20, 80), ' 7.5K').draw(win)
  Text(Point(20, 30), ' 10.0K').draw(win)

  # Draw the bar for the initial principal
  height = principal * 0.02
  bar = Rectangle(Point(40, 230), Point(65, 230-height))
  bar.setFill("green")
  bar.setWidth(2)
  bar.draw(win)
  for year in range(1,11):
    principal = (principal * (1 + rate))
    # draw the bar for the value
    x11 = year * 25 + 40
    height = principal * 0.02
    bar = Rectangle(Point(x11, 230), Point(x11 + 25, 230 - height))
    bar.setFill("green")
    bar.setWidth(2)
    bar.draw(win)

  print("This is the amount you will have after 10 years:", round(principal,2))
  input("Press <Enter> to quit")
  win.close()

main()