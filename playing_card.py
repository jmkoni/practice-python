class PlayingCard:
  def __init__(self, rank, suit):
    self.rank = rank
    self.suit = suit[0].lower

  def getRank(self):
    if self.rank == 1:
      return "Ace"
    elif self.rank < 11:
      return self.rank
    elif self.rank == 11:
      return "Jack"
    elif self.rank == 12:
      return "Queen"
    elif self.rank == 13:
      return "King"

  def getSuit(self):
    # case self.suit:
    #   when "d":
    #     suit = "Diamonds"
    #   when "h":
    #     suit = "Hearts"
    #   when "c":
    #     suit = "Clubs"
    #   when "s":
    #     suit = "Spades"
    if self.suit == "d":
      suit = "Diamonds"
    elif self.suit == "h":
      suit = "Hearts"
    elif self.suit == "c":
      suit = "Clubs"
    else:
      suit = "Spades"
    return suit

  def BJValue(self):
    if self.rank < 11:
      return self.rank
    else:
      return 10

  def __str__(self):
    self.getRank + " of " + self.getSuit

