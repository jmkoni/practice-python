def hotelCost(days):
    return 140*days

def planeRideCost(city):
    if city == "Charlotte":
        return 183 
    elif city == "Tampa":
        return 220 
    elif city == "Pittsburgh": 
        return 222
    elif city == "Los Angeles":
        return 475

def rentalCarCost(days):
    cost = days*40
    if days >= 7:
        cost -= 50
    elif days >= 3:
        cost -= 20
    return cost 

def tripCost(city,days,cash=0):
    return cash+\
    rentalCarCost(days)+\
    hotelCost(days)+\
    planeRideCost(city)

# You were planning on taking a trip to LA for 5 days 
# with $600 of spending money. 
est = tripCost("Los Angeles",5,600)
diff = 2734.23 - est
print diff
