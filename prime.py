from math import ceil, sqrt
def main():
  # a positive whole number n > 2 is prime if no number between 2 and sqrt(n) evenly divides n
  num = eval(input("Please enter a number >> "))
  if sqrt(num) == 2:
    isPrime = "is not"
  else:
    isPrime = "is"
    square = ceil(sqrt(num))
    for i in range(2, square):
      print(i)
      if num%i == 0:
        isPrime = "is not"
        break

  print(num, isPrime, "a prime number.")

main()
