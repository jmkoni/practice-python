import math

def main():
  print("This program finds the real solutions to a quadratic.")
  print()

  a, b, c = eval(input("Please enter the coefficients (a, b, c): "))
  base = b**2 - (4 * a * c)
  if base < 0:
    print("Unable to find a solution.")
  elif base == 0:
      root = -b / (2 * a)
      print("There is a double root at", root)
  else:
      discRoot = math.sqrt(base)
      root1 = (-b + discRoot) / (2 * a)
      root2 = (-b - discRoot) / (2 * a)
      print()
      print("The solutions are:", root1, root2)

main()