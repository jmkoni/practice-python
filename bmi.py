def main():
  # calculate bmi
  weight = eval(input("Please enter you weight: "))
  height = eval(input("Please enter your height in inches: "))

  bmi = (weight * 720) / (height**2)
  print("Your bmi is: ", bmi)
  if bmi < 19:
    print("It is below the healthy range.")
  elif bmi > 25:
    print("It is above the healthy range.")
  else:
    print("It is within the healthy range.")


main()