from gpa import Student, makeStudent

def readStudents(filename):
  infile = open(filename, 'r')
  students = []
  for line in infile:
    students.append(makeStudent(line))
  infile.close()
  return students

def writeStudents(filename):
  # students is a list of student objects
  outfile = open(filename, 'w')
  for s in students:
    print("{0}\t{1}\t{2}".format(s.getName(), s.getHours(), s.getQPoints()), file=outfile)
  outfile.close()

def use_gpa(aStudent):
  return aStudent.gpa()


def main():
  filename = input("Enter the name of the grade file: ")
  students = readStudents(filename)

  students.sort(key=use_gpa)

  filename = input("Enter a name for the output file: ")
  writeStudents(students, filename)
  print("The data has been written to", filename)

if __name__ == '__main__':
  main()
    