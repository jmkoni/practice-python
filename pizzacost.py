import math
def main():
  print("This program calculates the cost of a pizza given its diameter and price.")
  diameter = eval(input("Please enter the diameter of the pizza: "))
  price = eval(input("Please enter the price: "))
  area = math.pi * (diameter / 2)**2
  cost_per_inch = price / area
  print("This pizza costs", cost_per_inch, "dollars per square inch.")

main()