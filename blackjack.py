from random import randint

def main():
  printIntro()
  n = getNumGames()
  busted = simNGames(n)
  printSummary(busted, n)

def printIntro():
  print("This program simulates a number of games of blackjacks and gives the probability")
  print("of the dealer busting.")

def getNumGames():
  n = eval(input("Please enter the number of games you wish to simulate: "))
  return n

def simNGames(n):
  busted = 0
  for i in range(n):
    bust = simOneGame()
    if bust:
      busted += 1
  return busted

def simOneGame():
  hasAce = False
  hand = 0
  bust = False
  while hand < 17:
    card = randint(1,10)
    if card == 1:
      hasAce = True
    hand += card
    hand = aceValue(hasAce, hand)
  if hand > 21:
    bust = True
  return bust

def aceValue(hasAce, hand):
  tempHand = hand
  if hasAce:
    if tempHand < 17:
      tempHand += 10
      if (tempHand >= 17) and (tempHand <= 21):
        hand = tempHand
  return hand

def printSummary(busted, n):
  print("Dealer busted", busted, "times, {0:0.1%} of the games.".format(busted/n))


if __name__ == '__main__': main()