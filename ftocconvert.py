from tabulate import tabulate

def main():
  print("Table of temperatures C to F")
  table = [["Celsius", "Fahrenheit"],["-------","-----------"]]
  for i in range(11):
    celsius = i * 10
    fahrenheit = 9/5 * celsius + 32
    table.append([celsius, fahrenheit])

  print(tabulate(table))

main()