class Student:
  """creates a new student to calculate gpa"""
  def __init__(self, name, hours=0, qpoints=0):
    self.name = name
    self.hours = float(hours)
    self.qpoints = float(qpoints)

  def getName(self):
    return self.name

  def getHours(self):
    return self.hours

  def getQPoints(self):
    return self.qpoints

  def gpa(self):
    return self.qpoints/self.hours

  def addGrade(self, gradePoint, credits):
    self.hours += credits
    self.qpoints += gradePoint * credits

def makeStudent(infoStr):
  # infoStr is a tab-separated line: name hours qpoints
  name, hours, qpoints = infoStr.split("\t")
  return Student(name, hours, qpoints)
  
def main():
  filename = input("Enter the name of the grade file: ")
  infile = open(filename, 'r')

  best = makeStudent(infile.readline())

  for line in infile:
    s = makeStudent(line)
    if s.gpa() > best.gpa():
      best = s

  infile.close()
  
  print("The best student is:", best.getName())
  print("hours:", best.getHours())
  print("GPA:", best.gpa())

if __name__ == '__main__':
  main()
    