from graphics import *
def main():
  print("This program graphs out the future value of your investment.")
  principal = eval(input("Please enter the principal balance: "))
  rate = eval(input("Please enter the yearly interest rate: "))

  # Create the graphics window
  win = GraphWin("Investment Growth Chart", 320, 240)
  win.setBackground("white")
  win.setCoords(-1.75, -200, 11.5, 10400)
  Text(Point(-1, 0), ' 0.0K').draw(win)
  Text(Point(-1, 2500), ' 2.5K').draw(win)
  Text(Point(-1, 5000), ' 5.0K').draw(win)
  Text(Point(-1, 7500), ' 7.5K').draw(win)
  Text(Point(-1, 10000), ' 10.0K').draw(win)

  # Draw the bar for the initial principal
  drawBarForPrincipal(win, 0, principal)

  for year in range(1,11):
    principal = (principal * (1 + rate))
    # draw the bar for the value
    x11 = year * 25 + 40
    height = principal * 0.02
    drawBarForPrincipal(win, year, principal)

  print("This is the amount you will have after 10 years:", round(principal,2))
  input("Press <Enter> to quit")
  win.close()

def drawBarForPrincipal(window, year, height):
  bar = Rectangle(Point(year, 0), Point(year + 1, height))
  bar.setFill("green")
  bar.setWidth(2)
  bar.draw(window)

main()