def main():
  animals = {"cow": "moo", "horse": "neigh", "sheep": "baa", "dog": "bark", "cat": "meow"}

  for k,v in animals.items():
    oldMac()
    oldMacAnimals(k,v)
    oldMac()
    print("")

def oldMac():
  print("Old MacDonald had a farm, Ei-igh, Ei-igh, Oh!")

def oldMacAnimals(animal, noise):
  print("And on that farm he had a {0}, Ei-igh, Ei-igh, Oh!\nWith a {1}, {1} here and a {1}, {1} there.\nHere a {1}, there a {1}, everywhere a {1}, {1}.".format(animal, noise))

main()