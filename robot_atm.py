import math
# simulates an ATM withdrawal
# withdrawals must be divisible by 5, .5 + 1% commision
def robot_atm(data):
	balance, withdrawal = data
	for amt in withdrawal:
		if (amt % 5 == 0) & (amt >= 0):
			wdwl = amt + .5 + (.01*amt)
			if wdwl <= balance:
				balance = math.floor(balance - wdwl)
	return balance


print robot_atm([120, [10, 20, 30]])
print robot_atm([120, [200, 10]])
print robot_atm([120, [3, 10]])
print robot_atm([120, [200, 119]])
print robot_atm([120, [120, 10, 122, 2, 10, 10, 30, 1]])