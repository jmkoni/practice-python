def main():
  print("This program converts strings to dates")
  date = input("Please enter the date in mm/dd/yyyy format: ")
  month, day, year = date.split("/")
  monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "October", "November", "December"]
  month = monthNames[int(month)-1]

  print("The date is", month, day, ",", year)

main()