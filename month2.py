def main():
  print("This program outputs the abbreviation for a given month.\n")

  # string of month abbreviations
  months = ["Jan","Feb","Mar","Apr","May","Jun",
            "Jul","Aug","Sep","Oct","Nov","Dec"]

  # get user input
  desiredMonth = input("Please enter a month number (1-12): ")
  if desiredMonth.isdigit():
    monthAbbrev = months[int(desiredMonth)-1)]
    print("The abbreviation for month", desiredMonth, "is",monthAbbrev)
  else:
    print("Please enter a number.")

main()