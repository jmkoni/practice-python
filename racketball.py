from random import random

def main():
  printIntro()
  prob = eval(input("Please enter the percentage of time your opponent wins serves: "))
  score1 = 0
  score2 = 0
  while (score1 < 15) and (score2 < 15):
    if random() < prob:
      score1 = score1 + 1
    else:
      score2 = score2 + 1

  if score1 > score2:
    print("Your opponent won,", score1, "to", score2)
  else:
    print("You won,", score2, "to", score1)

def printIntro():
  print("This program simulates a single racketball game given an expected win probability for each player.")
main()