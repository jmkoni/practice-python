def main():
  nums = eval(input("Please enter a series of comma separated numbers: "))
  sum = 0.0
  i = 0
  while i < len(nums):
    sum = sum + nums[i]
    i += 1

  print("\nThe average of the numbers is", sum / len(nums))

main()