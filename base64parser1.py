#########
# Parse list of base64 encoded URLs
#########
from __future__ import with_statement
from urlparse import urlparse

import time
import base64
import commands
import urllib
import urllib2
import json
from xml.etree import ElementTree as ET

password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
password_mgr.add_password(None, 'https://dbl.api-test.spamhaus.org/', 'xxxxxx', 'xxxxxxxx')
handler = urllib2.HTTPBasicAuthHandler(password_mgr)
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
print "url,status,score"

# load tlds, ignore comments and empty lines:
with open("effective_tld_names.txt") as tldFile:
    tlds = [line.strip() for line in tldFile if line[0] not in "/\n"]

file = open('base64urls.txt')

#this code thanks to Markus - http://stackoverflow.com/questions/1066933/how-to-extract-domain-name-from-url
def getDomain(url, tlds):
    urlElements = urlparse(url)[1].split('.')
    # urlElements = ["abcde","co","uk"]

    for i in range(-len(urlElements),0):
        lastIElements = urlElements[i:]
        #    i=-3: ["abcde","co","uk"]
        #    i=-2: ["co","uk"]
        #    i=-1: ["uk"] etc

        candidate = ".".join(lastIElements) # abcde.co.uk, co.uk, uk
        wildcardCandidate = ".".join(["*"]+lastIElements[1:]) # *.co.uk, *.uk, *
        exceptionCandidate = "!"+candidate

        # match tlds:
        if (exceptionCandidate in tlds):
            return ".".join(urlElements[i:])
        if (candidate in tlds or wildcardCandidate in tlds):
            return ".".join(urlElements[i-1:])
            # returns "abcde.co.uk"

    return url + ",Domain not in global list of TLDs"

for line in file:
	urls = line.split(', ')
	for s in urls:
		result = base64.b64decode(s)
		stripResult = getDomain(result,tlds)
		if 'Domain not in' in stripResult:
			print stripResult
		else:
			requestURL = 'https://dbl.api-test.spamhaus.org/2012-09-12/domain/?domain=' + stripResult
			f = urllib2.urlopen(requestURL)
			jdata = json.load(f)
			if jdata['status']=="not found":
				print stripResult + ',' + jdata['status'] + ',n/a'
			elif jdata['score']==0:
				print stripResult + ',' + jdata['status'] + ',0,' + jdata['spamtrap_hits'] + ',' + jdata['last_seen']
			else:
				print stripResult + ',' + jdata['status'] + ',' + jdata['score'] + ',' + jdata['spamtrap_hits'] + ',' + jdata['last_seen']
		time.sleep(1)
