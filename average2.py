def main():
  count = 0
  sum = 0.0
  moredata = "yes"
  while moredata[0] == "y":
    num = eval(input("Enter a number >> "))
    sum = sum + num
    count += 1
    moredata = input("Do you have more numbers (yes or no)? ")

  print("\nThe average of the numbers is", sum / count)

main()