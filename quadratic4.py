import math

def main():
  print("This program finds the real solutions to a quadratic.")
  print()

  try:
    a, b, c = eval(input("Please enter the coefficients (a, b, c): "))
    base = b**2 - (4 * a * c)
    discRoot = math.sqrt(base)
    root1 = (-b + discRoot) / (2 * a)
    root2 = (-b - discRoot) / (2 * a)
    print()
    print("The solutions are:", root1, root2)
  except ValueError as excObj:
    if str(excObj) == "math domain error":
      print("\nNo real roots.")
    else:
      print("\nPlease enter the right number of coefficients.")
  except NameError:
    print("\nPlease enter three numbers.")
  except TypeError:
    print("\nPlease enter only numbers.")
  except SyntaxError:
    print("\nThe input was not in the correct form. Missing comma.")
  except:
    print("\nSomething went wrong. Sorry!")

main()