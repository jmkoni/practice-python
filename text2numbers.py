def main():
  print("Converts a message to a sequence of numbers, utilizing the underlying unicode encoding.")

  # Get the message to encode
  message = input("Please enter the message to encode: ")

  print("\nHere are the Unicode codes: ")

  #Loop through the message and print out the Unicode values
  for ch in message:
    print(ord(ch), end=" ")

  print()

main()