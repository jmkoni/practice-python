def main():
  print("This program generates computer usernames from a mfile of names.\n")

  # get the file names
  infileName = input("What file are the names in? ")
  outfileName = input("What file should the usernames go in? ")

  #open the files
  infile = open(infileName, "r")
  outfile = open(outfileName, "w")


  # process each line of the input file
  for line in infile:
    first, last = line.split()
    uname = (first[0] + last[:7]).lower()
    print(uname, file=outfile)

  #close the files
  infile.close()
  outfile.close()

  #output
  print("Usernames have been written to", outfileName)

main()