class Part(object)
	def __init__(self, description):
		self.description = description

class Tire(Part):
	def __init__(self, description, miles_warranty):
		Part.__init__(self, description)
		self.miles_warranty = miles_warranty

class Car(object):
	def __init__(self, vin):
		self.vin = vin
		self.steering_wheel = Part('steering wheel')
		self.tires = [Tire('tire %d' % i, 3000) for i in range(4)]

car = Car('123')
print(car.steering_wheel.description)
for tire in car.tires:
	print('%s: %d' % (tire.description, tire.miles_warranty))

