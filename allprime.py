from math import ceil, sqrt
def main():
  # a positive whole number n > 2 is prime if no number between 2 and sqrt(n) evenly divides n
  num = eval(input("Please enter a number >> "))
  primes = []
  for i in range(2, num):
    if sqrt(i) == 2:
      isPrime = False
    else:
      isPrime = True
      square = ceil(sqrt(i))
      for j in range(2, square):
        if i%j == 0:
          isPrime = False
          break
    if isPrime:
      primes.append(i)

  prime_string = ", ".join(str(int(i)) for i in primes)
  print("Here are all the prime numbers up to", num,":", prime_string)

main()