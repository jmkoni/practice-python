# Guess the number game using simplegui module made for Coursera Interactive Python course
import simplegui
import random
import math

# initialize global variables used in your code
def init():
    global comp_number
    global num_guess
    # print "Pick a range!"
    print "Starting game with range of 0 to 100."
    range100()

# define event handlers for control panel
def num_guess_avail(up,low):
    num_guess = math.ceil(math.log((up-low+1),2))
    return num_guess

def range100():
    # button that changes range to range [0,100) and restarts
    global comp_number, num_guess
    up = 99
    low = 0
    comp_number = random.randint(low, up)
    num_guess = num_guess_avail(up,low)
    print " "
    print "New game! Guess numbers between 0 and 100"
    print "You have %d guesses." % num_guess

def range1000():
    # button that changes range to range [0,1000) and restarts
    global comp_number, num_guess
    low = 0
    up = 999
    comp_number = random.randint(low, up)
    num_guess = num_guess_avail(up,low)
    print " "
    print "New game! Guess numbers between 0 and 1000"
    print "You have %d guesses." % num_guess

def get_input(guess):
    # main game logic goes here	
    global num_guess
    num_guess = num_guess - 1
    guess = int(guess)
    print "You guessed %d." % guess
    if guess < comp_number:
        print "higher!"
        print "You have %d guess(es) left." % num_guess
        print " "
    elif guess > comp_number:
        print "lower!"
        print "You have %d guess(es) left." % num_guess
        print " "
    elif guess == comp_number:
        print "correct!"
        init()
    else:
        print "Please enter a number."
    if num_guess == 0:
        print "You missed it :("
        init()
    
# create frame
f = simplegui.create_frame("Guess the number",300,300)

# register event handlers for control elements
f.add_button("Range is [0,100)", range100, 200)
f.add_button("Range is [0,1000)", range1000, 200)
f.add_input("Enter a guess:", get_input, 200)

# start frame
init()
f.start()

