def main():
  print("This program takes a temperature in Celsius and converts it to Fahrenheit.")

  celsius = eval(input("What is the temperature in Celsius? "))
  fahrenheit = 9/5 * celsius + 32

  if fahrenheit > 90:
    print("It is extremely hot out today.")
  if fahrenheit < 20:
    print("It is extremely cold out today.")

  print("The temperature is", fahrenheit, "degrees Fahrenheit.")

main()