#!/usr/bin/python
# -*- coding: UTF-8 -*-
# 
# Write a class or function that computes the histogram of a series of numbers
# 
# It should take an array as an input and return an array of the unique inputs and their counts.
# 
# Example:
#   Input: [22,33,22,44,44,44,22,33,33,33,33]
#   Output: [ [22,3], [33,5], [44,3]]

from collections import defaultdict
import sys
import json


def NumCount(array):
	#used defaultdict to lose a few lines when adding new values
	nc = defaultdict(int)
	try:
		#separates out json (due to form that array is passed in)
		numlist = json.loads("".join(array))
		#for each number in the list, add one to the count
		for num in numlist:
			nc[num] += 1
		#prints to result to screen
		print nc.items()
	# catches if a user tries to pass an array in the wrong format
	except ValueError:
		print "That was not formatted correctly. Please input a numberical list/array in the form [1,2,3]"
		y=raw_input("> ")
		NumCount(y)

def GetNum(x):
	#if an array is not inputed on the command line, prompt for one 
	if x == []:
		print "Please input a numberical list/array in the form [1,2,3]"
		y = raw_input("> ")
		NumCount(y)
	#expected length of input is one ['[1,2,3']']
	elif len(x)==1:
		NumCount(x)
	else:
		print "That does not seem to be the correct format. Please input a numberical list/array in the form [1,2,3]"
		y = raw_input("> ")
		NumCount(y)

#calls function ignoring first argument (function name)
GetNum(sys.argv[1:])