def main():
  print("Converts a sequence of numbers to characters, utilizing the underlying unicode encoding.")

  # Get the message to encode
  sequence = input("Please enter the sequence of numbers to decode: ")

  # initialize the message
  chars = []
  #Loop through the message and print out the Unicode values
  for ch in sequence.split():
    if ch.isdigit():
      chars.append(chr(int(ch)))
      # print(chr(int(ch)), end="")
    else:
      print("Please enter a sequence of numbers, not letters.")
      return

  message = "".join(chars)
  print("\nHere is the message:" message)

main()