import math

class Sphere:
  """Creates a sphere"""
  def __init__(self, radius):
    self.radius = radius

  def getRadius(self):
    return self.radius

  def surfaceArea(self):
    sa = (4 * math.pi * self.radius**2)
    return sa

  def volume(self):
    vol = (4/3 * math.pi * self.radius**3)
    return vol


def main():
  print("This program calculates the volume and surface area of a sphere from its radius.")
  r = eval(input("Please enter the radius of the sphere: "))
  if r < 0:
    print("Please enter a positive number.")
    return
  s = Sphere(r)
  print("The volume of the sphere is", s.volume())
  print("The area of the sphere is", s.surfaceArea())

main()
