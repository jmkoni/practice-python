def byFreq(pair):
  return pair[1]

def prepareFile():
  fname = input("File to analyze: ")
  text = open(fname, "r").read()
  text = text.lower()

  for ch in '!"#$%&()*+,-./;:<=>?@[\\]^_\'{|}~':
    text = text.replace(ch, " ")

  return text.split()

def countWords(words):
  counts = {}
  for w in words:
    counts[w] = counts.get(w,0) + 1

def main():
  words = prepareFile()
  counts = countWords(words)

  n = eval(input("Output analysis of how many words? "))
  items = list(counts.items())
  items.sort()
  items.sort(key=byFreq, reverse=True)
  for i in range(n):
    word, count = items[i]
    print("{0:<15}{1:>5}".format(word, count))

if __name__=='__main__': main()