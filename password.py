import re

def password(data):
	length = len(data) >= 10
	up_low = False
	low = re.search('[a-z]', data)
	high = re.search('[A-Z]', data)
	number = re.search('[0-9]', data)
	if low:
		if high:
			if number:
				up_low = True
	valid = length & up_low
	return valid

# This is what I tried the first time which for some reason kept failing when using lowercase.	
	# if map(str.isupper,data).index(True)
	# try:
	# 	map(str.isupper,data).index(True)
	# 	try:
	# 		map(str.islower,data).index(True)
	# 		try:
	# 			map(str.isdigit,data).index(True)
	# 			up_low = True
	# 		except:
	# 			up_low = False
	# 	except:
	# 		up_low = False
	# except:
	# 	up_low = False
	# valid = length & up_low
	# return valid


if __name__ == '__main__':
    assert password('A1213pokl') == False, "1st example"
    assert password('bAse730onE4') == True, "2nd example"
    assert password('asasasasasasasaas') == False, "3rd example"
    assert password('QWERTYqwerty') == False, "4th example"
    assert password('123456123456') == False, "5th example"
    assert password('QwErTy911poqqqq') == True, "6th example"
#     
print password('A1213pokl')
print password('bAse730onE4')
print password('asasasasasasasaas')
print password('QWERTYqwerty')
print password('123456123456')
print password('QwErTy911poqqqq')
print password('ULFFunH8ni')