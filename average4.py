def main():
  count = 0
  sum = 0.0
  num = input("Enter a number (Enter to quit) >> ")
  while num != "":
    num = eval(num)
    sum = sum + num
    count += 1
    num = input("Enter a number (Enter to quit) >> ")

  print("\nThe average of the numbers is", sum / count)

main()