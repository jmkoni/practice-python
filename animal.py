class Animal(object):
  def __init__(self, name, species, legs=4):
    self.name = name
    self.species = species
    self.legs = legs

  def __str__(self):
    return "%s is a %s" % (self.name, self.species)

class Dog(Animal):
  def __init__(self, name, chases_cats, drools, legs=4):
    Animal.__init__(self, name, "Dog")
    self.chases_cats = chases_cats
    self.drools = drools

  def chasesCats(self):
    return self.chases_cats

  def doesDrool(self):
    return self.drools

class Cat(Animal):
  def __init__(self, name, scared_of_dogs, fat, goes_outside):
    Animal.__init__(self, name, "Cat")
    self.scared_of_dogs = scared_of_dogs
    self.fat = fat
    self.goes_outside = goes_outside

  def scared_of_dogs(self):
    return self.scared_of_dogs

  def isFat(self):
    return self.fat

  def goesOutside(self):
    return self.goes_outside