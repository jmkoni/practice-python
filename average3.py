def main():
  count = 0
  sum = 0.0
  num = eval(input("Enter a number (negative to quit) >> "))
  while num >= 0:
    sum = sum + num
    count += 1
    num = eval(input("Enter a number (negative to quit) >> "))

  print("\nThe average of the numbers is", sum / count)

main()