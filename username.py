def main():
  print("This program generates computer usernames.\n")

  # get user's first and last names
  first = input("Please enter your first name: ")
  last = input("Please enter your last name: ")

  #concatenate first initial with 7 chars of last name
  uname = first[0].lower() + last[:7].lower()

  #output
  print("Your username is:", uname)

main()