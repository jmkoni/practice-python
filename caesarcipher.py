def main():
  print("This program uses a Caesar cipher to encode messages.")

  message = input("Please enter the message you want encoded: ")
  key = eval(input("Please enter the key (integer): "))
  encodedMessage = []

  for char in message:
    encodedMessage.append(chr(ord(char) + key))

  print("Your encoded message is:", "".join(encodedMessage))

main()