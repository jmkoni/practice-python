def main():
  # computes total wages
  hours = eval(input("Please enter number of hours worked this week: "))
  wage = eval(input("Please enter the hourly wage: "))

  over = hours - 40
  extra = 0 if over < 0 else over
  total = (hours * wage) + (extra * wage * 1.5)

  print("Total earned this week: ${0:0.2f}".format(total))


main()