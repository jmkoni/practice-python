#!/usr/bin/python
# -*- coding: UTF-8 -*-
# You should create 2 "workers" in the language of your choice- each should be a standalone script that interact with a locally running instance of the redis server
# 
# worker1  
# 	generates a random number once every second and adds it to a queue for worker2 to process
# 
# worker2  
# 	keeps a redis key called 'total' updated with the running sum of all numbers that appear in its queue 

import redis
import random
import time

#sets redis server to point to one running on local host
rserver = redis.Redis("localhost")
#if this already exists on server, delete
rserver.delete("randomNum")
rserver.delete("total")
#instantiates randomNum with a random int between 0 and 100
rserver.rpush("randomNum", random.randint(0,100))
#instantiates total with random int
rserver.set("total", int(rserver.lindex("randomNum", int(rserver.llen("randomNum"))-1)))
#sets up variable randTot = random int
randTot = int(rserver.lindex("randomNum", int(rserver.llen("randomNum"))-1))
#continues looping while there are less than x random numbers in the queue
while rserver.llen("randomNum") < 100:
	#generates another random number, adds to queue
	rserver.rpush("randomNum", random.randint(0,100))
	#adds random number to queue
	randTot += int(rserver.lindex("randomNum", int(rserver.llen("randomNum"))-1))
	#wanted to use incrby, but for reasons i couldn't find, it would not work with redis-py. incr worked...
	#rserver.incrby("randTot",i)
	#sets total = randTotal (current sum)
	rserver.set("total", randTot)
	#print rserver.get("total")
	#print rserver.lrange("randomNum", 0, -1)
	#pause for one second before continuing
	time.sleep(1)