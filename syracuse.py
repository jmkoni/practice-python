def main():
  # Syracuse Sequence
  # syr(x) = x / 2 if x is even
  # else... 3x + 1
  start = eval(input("Please enter a number to start the Syracuse Sequence >> "))
  x = start
  sequence = []
  while x != 1:
    if x%2 == 0:
      x = x / 2
    else:
      x = 3 * x + 1
    sequence.append(x)

  seq_string = ", ".join(str(int(i)) for i in sequence)
  print("The Syracuse Sequence starting with", start, "is", seq_string)

main()
