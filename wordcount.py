def main():
  print("This program outputs the word count, line count, and character count in a file.")
  fname = input("Enter filename: ")
  infile = open(fname, "r")
  wordCount = 0
  lineCount = 0
  charCount = 0
  for line in infile:
    lineCount += 1
    for word in line.split(" "):
      wordCount += 1
      charCount += len(word)

  print("In this file there are {0} characters, {1} words, and {2} lines."
        .format(charCount, wordCount, lineCount))


main()