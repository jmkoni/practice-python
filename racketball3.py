from random import random

def main():
  printIntro()
  probA, probB, n = getInputs()
  winsA, winsB, soA, soB = simNGames(n, probA, probB)
  printSummary(winsA, winsB, soA, soB)

def printIntro():
  print("This program simulates a game of racquetball between two")
  print("players called \"A\" and \"B\". The abilities of each player")
  print("is indicated by a probability (a number between 0 and 1) that")
  print("the player wins the points when serving. Player A always")
  print("has the first serve.")

def getInputs():
  a = eval(input("Please enter the percentage of time player A wins serves: "))
  b = eval(input("Please enter the percentage of time player B wins serves: "))
  n = eval(input("Please enter the number of games you wish to simulate: "))
  return a, b, n

def simNGames(n, probA, probB):
  winsA, winsB, soA, soB = 0,0,0,0
  for i in range(n):
    scoreA, scoreB = simOneGame(probA, probB)
    if scoreA > scoreB:
      if scoreA == 7:
        soA += 1
      winsA += 1
    else:
      if scoreB == 7:
        soB += 1
      winsB += 1
  return winsA, winsB, soA, soB

def simOneGame(probA, probB):
    scoreA, scoreB = 0,0
    serving = "A"
    while not gameOver(scoreA, scoreB):
      if serving == "A":
        if random() < probA:
          scoreA += 1
        else:
          serving = "B"
      else:
        if random() < probB:
          scoreB += 1
        else:
          serving = "A"

    return scoreA, scoreB

def gameOver(a, b):
  return a==15 or b==15 or (a == 7 and b == 0) or (b ==7 and a == 0)

def printSummary(a, b, soA, soB):
  n = a + b
  print("\nGames simulated:", n)
  print("Wins for Player A: {0} ({1:0.1%})".format(a, a/n))
  print("Wins for Player B: {0} ({1:0.1%})".format(b, b/n))
  print("Player A was shutout", soB, "times, {0:0.1%} of the games.".format(soB/n))
  print("Player B was shutout", soA, "times, {0:0.1%} of the games.".format(soA/n))

if __name__ == '__main__': main()