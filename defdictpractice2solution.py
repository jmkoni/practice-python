from collections import defaultdict
from operator import itemgetter
freqcount = defaultdict(int)
linenumbers = defaultdict(list)
lengths = defaultdict(set)

f = open('blue.txt','r')
for i,line in enumerate(f):
    words = line.split()
    for word in words:
        freqcount[word] += 1
        linenumbers[word].append(i)
        lengths[len(word)].add(word)

print sorted(freqcount.items(), key = itemgetter(1), reverse=True)[:20]
        
