def main():
  print("Computer Fibonacci up to the nth number")
  n = eval(input("Please enter the place of the Fibonacci number that you would like: "))
  for r in fib(n):
    f = r
  print("The", n, "th value of the Fibonacci sequence is", f)

def fib(n):
    old = 0
    cur = 1
    i = 1
    yield cur
    while (i < n):
        cur, old, i = cur+old, cur, i+1
        yield cur

main()
