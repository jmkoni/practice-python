import math import sqrt

def mean(nums):
  sum = 0.0
  for num in nums:
    sum += num
  return sum / len(nums)

def getNumbers():
  nums = []
  num = input("Enter a number (Enter to quit) >> ")
  while num != "":
    num = eval(num)
    nums.append(num)
    num = input("Enter a number (Enter to quit) >> ")
  return nums

def median(nums):
  nums.sort()
  size = len(nums)
  midPos = size // 2
  if size % 2 == 0:
    median = (nums[midPos] + nums[midPos-1]) / 2
  else:
    median = nums[midPos]
  return median

def stdDev(nums, xbar):
  sumDevSq = 0.0
  for num in nums:
    dev = xbar - num
    sumDevSq += dev**2
  return sqrt(sumDevSq/(len(nums) - 1))

def main():
  data = getNumbers()
  xbar = mean(data)
  std = stdDev(data, xbar)
  med = median(data)
  print("\nThe mean is", xbar)
  print("\nThe standard deviation is", std)
  print("\nThe median is", med)

if __name__ == '__main__':
  main()