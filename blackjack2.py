from random import randint

def main():
  printIntro()
  n = getNumGames()
  bustDict = simNGames(n)
  printSummary(bustDict, n)

def printIntro():
  print("This program simulates a number of games of blackjacks and gives the probability")
  print("of the dealer busting.")

def getNumGames():
  n = eval(input("Please enter the number of games you wish to simulate: "))
  return n

def simNGames(n):
  bustDict = {}
  for k in range(1,11):
    busted = 0
    for i in range(n):
      bust = simOneGame(k)
      if bust:
        busted += 1
    bustDict[k] = busted
  return bustDict

def simOneGame(starter):
  if starter == 1:
    hasAce = True
  else:
    hasAce = False
  hand = starter
  bust = False
  while hand < 17:
    card = randint(1,10)
    if card == 1:
      hasAce = True
    hand += card
    hand = aceValue(hasAce, hand)
  if hand > 21:
    bust = True
  return bust

def aceValue(hasAce, hand):
  tempHand = hand
  if hasAce:
    if tempHand < 17:
      tempHand += 10
      if (tempHand >= 17) and (tempHand <= 21):
        hand = tempHand
  return hand

def printSummary(bustDict, n):
  for card, busted in bustDict.items():
    print("When starting with a {0}, dealer busted {1} times, {2:0.1%} of the games.".format(card, busted, busted/n))


if __name__ == '__main__': main()