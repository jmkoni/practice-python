from tabulate import tabulate
#problem 8.2
def main():
  print("Table of Windchill Temperatures")
  print("LEGEND: rows - Wind Speed, columns - Temperature")
  table = [[""], ["---"]]
  #populate temperature columns
  for i in range(-20, 70, 10):
    table[0].append(i)
    table[1].append("----")
  count = 2
  for w in range(0, 55, 5):
    tempWind = [w]
    for temp in table[0]:
      if temp != "":
        windchill = 35.74 + (0.6215 * temp) - (35.75 * (w**(0.16))) + (0.4275 * temp * (w**(0.16)))
        tempWind.append(round(windchill,2))
    table.append(tempWind)

  print(tabulate(table))

main()