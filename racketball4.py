from random import random
# import pdb;pdb.set_trace()

class SimStats:
  def __init__(self):
    self.winsA = 0
    self.winsB = 0
    self.shutsA = 0
    self.shutsB = 0

  def update(self, aGame):
    a, b = aGame.getScores()
    if a > b:
      self.winsA += 1
      if b == 0:
        self.shutsA += 1
    else:
      self.winsB += 1
      if a == 0:
        self.shutsB += 1

  def printReport(self):
    n = self.winsA + self.winsB
    print("Summary of", n, "games:\n")
    print("          wins (% total)  shutouts  (% wins)  ")
    print("--------------------------------------------")
    self.printLine("A", self.winsA, self.shutsA, n)
    self.printLine("B", self.winsB, self.shutsB, n)

  def printLine(self, label, wins, shuts, n):
    template = "Player {0}:{1:5}  ({2:5.1%}){3:11}    ({4})"
    if wins == 0:
      shutStr = "----"
    else:
      shutStr = "{0:4.1%}".format(float(shuts)/wins)
    print(template.format(label, wins, float(wins)/n, shuts, shutStr))

class RBallGame():
  def __init__(self, probA, probB):
    self.playerA = Player(probA)
    self.playerB = Player(probB)
    self.server = self.playerA

  def play(self):
    while not self.isOver():
      if self.server.winsServe():
        self.server.incScore()
      else:
        self.changeServer()

  def getScores(self):
    return self.playerA.getScore(), self.playerB.getScore()

  def changeServer(self):
    if self.server == self.playerA:
      self.server = self.playerB
    else:
      self.server = self.playerA

  def isOver(self):
    a, b = self.getScores()
    return a==15 or b==15 or (a == 7 and b == 0) or (b ==7 and a == 0)

class Player():
  def __init__(self, prob):
    self.prob = prob
    self.score = 0

  def getScore(self):
    return self.score

  def winsServe(self):
    return random() <= self.prob

  def incScore(self):
    self.score += 1

def main():
  printIntro()
  probA, probB, n = getInputs()
  stats = SimStats()
  for i in range(n):
    theGame = RBallGame(probA, probB)
    theGame.play()
    stats.update(theGame)
  stats.printReport()

def printIntro():
  print("This program simulates a game of racquetball between two")
  print("players called \"A\" and \"B\". The abilities of each player")
  print("is indicated by a probability (a number between 0 and 1) that")
  print("the player wins the points when serving. Player A always")
  print("has the first serve.")

def getInputs():
  a = eval(input("Please enter the percentage of time player A wins serves: "))
  b = eval(input("Please enter the percentage of time player B wins serves: "))
  n = eval(input("Please enter the number of games you wish to simulate: "))
  return a, b, n

if __name__ == '__main__': main()