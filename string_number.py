FIRST_TEN = ["zero", "one", "two", "three", "four", "five", "six", "seven",
             "eight", "nine"]
SECOND_TEN = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
              "sixteen", "seventeen", "eighteen", "nineteen"]
OTHER_TENS = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy",
              "eighty", "ninety"]
HUNDRED = "hundred"

#Your code here
#You can import some modules or create additional functions


def string_number(number):
	numstr = ""
	cent = number / 100
	if cent > 0:
		numstr = FIRST_TEN[cent] + " " + HUNDRED
	tens = number / 10 % 10
	ones = number % 10
	if tens == 1:
		numstr = numstr + " " + SECOND_TEN[ones]
	else:
		if tens > 1:
			numstr = numstr + " " + OTHER_TENS[tens - 2]
		if ones > 0:
			numstr = numstr + " " + FIRST_TEN[ones]
	numstr = numstr.strip()
	return numstr

#Some hints
#Don't forget strip whitespaces at the end of string
print string_number(4)
print string_number(133)
print string_number(12)
print string_number(101)
print string_number(212)
print string_number(40)

#These "asserts" using only for self-checking and not necessary for auto-testing
# if __name__ == '__main__':
# 	assert string_number(4) == 'four', "1st example"
# 	assert string_number(133) == 'one hundred thirty three', "2nd example"
# 	assert string_number(12) == 'twelve', "3rd example"
# 	assert string_number(101) == 'one hundred one', "4th example"
# 	assert string_number(212) == 'two hundred twelve', "5th example"
# 	assert string_number(40) == 'forty', "6th example"
