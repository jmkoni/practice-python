def main():
  print("This program outputs the abbreviation for a given month.\n")

  # string of month abbreviations
  months = "JanFebMarAprMayJunJulAugSeptOctNovDec"

  # get user input
  desiredMonth = input("Please enter a month number (1-12): ")
  if desiredMonth.isdigit():
    pos = (int(desiredMonth) - 1) * 3
    monthAbbrev = months[pos:(pos + 3)]
    print("The abbreviation for month", desiredMonth, "is",monthAbbrev)
  else:
    print("Please enter a number.")

main()