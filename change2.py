def main():
  print("Change Counter")
  print()
  print("Please enter the count of each coin type: ")
  quarters = eval(input("Quarters: "))
  dimes = eval(input("Dimes: "))
  nickels = eval(input("Nickels: "))
  pennies = eval(input("Pennies: "))
  total = (quarters * 25) + (dimes * 10) + (nickels * 5) + (pennies * 1)

  # 0:0.2f - 0: index, 0: take as much space as you need, 2: precision, 2 decimals, f: fixed point
  print("The value of your change is ${0}.{1:0>2}"
        .format(total//100, total%100))

main()