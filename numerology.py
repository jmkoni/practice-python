def main():
  print("Determines the \"value\" of a name.")
  name = input("Please enter a name: ").lower()
  value = 0
  nameArray = name.split()
  for name in nameArray:
    for l in name:
      value = value + ord(l) - 96

  print("The value of your name is {0}.".format(value))

main()