from flask import Flask
app = Flask(__name__)

@app.route("/")
def start():
  return "Date: June 2002. You're Amanda Clarke."

if __name__ == "__main__":
  app.run(debug=True)
