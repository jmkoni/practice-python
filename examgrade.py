def main():
  print("Prints out letter grade given exam grade.")
  grade = eval(input("Please enter your numeric grade: "))
  if not isinstance(grade,int):
    print("Please enter an integer.")
    return
  if grade > 89:
    letterGrade = "A"
  elif grade > 79:
    letterGrade = "B"
  elif grade > 69:
    letterGrade = "C"
  elif grade > 59:
    letterGrade = "D"
  else:
    letterGrade = "F"

  print("Your grade is {}.".format(letterGrade))

main()