from graphics import *

def main():
  win = GraphWin("Get Area of Rectangle")
  win.setCoords(0.0, 0.0, 10.0, 10.0)
  message = Text(Point(5, 0.5), "Click on two points")
  message.draw(win)

  # get input points
  p1 = win.getMouse()
  p1.draw(win)
  p2 = win.getMouse()
  p2.draw(win)

  # generate points based on the other points
  p3 = Point(p1.getX(),p2.getY())
  p3.draw(win)
  p4 = Point(p2.getX(),p1.getY())
  p4.draw(win)

  # draw rectangle
  rectangle = Polygon(p1, p3, p2, p4)
  rectangle.setFill("blue")
  rectangle.setOutline("cyan")
  rectangle.draw(win)

  # calculate area
  width = abs(p1.getX() - p2.getX())
  height = abs(p1.getY() - p2.getY())
  area = round(width * height, 2)

  # print area
  message2 = Text(Point(5, 1.5), "Area = " + str(area))
  message2.draw(win)
  message.setText("Click anywhere to quit.")
  win.getMouse()

main()