from random import random

def main():
  printIntro()
  probA, probB, n = getInputs()
  winsA, winsB = simNGames(n, probA, probB)
  printSummary(winsA, winsB)

def printIntro():
  print("This program simulates a game of racquetball between two")
  print("players called \"A\" and \"B\". The abilities of each player")
  print("is indicated by a probability (a number between 0 and 1) that")
  print("the player wins the points when serving. Player A always")
  print("has the first serve.")

def getInputs():
  a = eval(input("Please enter the percentage of time player A wins serves: "))
  b = eval(input("Please enter the percentage of time player B wins serves: "))
  n = eval(input("Please enter the number of games you wish to simulate: "))
  return a, b, n

def simNGames(n, probA, probB):
  winsA, winsB = 0,0
  for i in range(n):
    scoreA, scoreB = simOneGame(probA, probB)
    if scoreA > scoreB:
      winsA += 1
    else:
      winsB += 1
  return winsA, winsB

def simOneGame(probA, probB):
    scoreA, scoreB = 0,0
    serving = "A"
    while not gameOver(scoreA, scoreB):
      if serving == "A":
        if random() < probA:
          scoreA += 1
        else:
          serving = "B"
      else:
        if random() < probB:
          scoreB += 1
        else:
          serving = "A"

    return scoreA, scoreB

def gameOver(a, b):
  return a==15 or b==15

def printSummary(a, b):
  n = a + b
  print("\nGames simulated:", n)
  print("Wins for Player A: {0} ({1:0.1%})".format(a, a/n))
  print("Wins for Player B: {0} ({1:0.1%})".format(b, b/n))

if __name__ == '__main__': main()