# first example from Zelle's Python Programming
def main():
  print("This program illustrates a chaotic function.")
  x = eval(input("Enter a number between 0 and 1: "),{"__builtins__":None},{})
  x1 = eval(input("Enter a second number between 0 and 1: "),{"__builtins__":None},{})
  n = eval(input("How many numbers should I print? "),{"__builtins__":None},{})
  y = eval(input("What is your desired chaos multiplier? "),{"__builtins__":None},{})
  first = [str(x)]
  second = [str(x1)]
  for i in range(n):
    x = y * x * (1-x)
    x1 = y * x * (1-x)
    first.append(str(x))
    second.append(str(x1))

  for row in list(zip(first,second)):
    print ' '.join([str(i) for i in row])

main()