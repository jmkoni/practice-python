class Student:
  """creates a new student to calculate gpa"""
  def __init__(self, name, hours, qpoints):
    self.name = name
    self.hours = float(hours)
    self.qpoints = float(qpoints)

  def getName(self):
    return self.name

  def getHours(self):
    return self.hours

  def getQPoitns(self):
    return self.qpoints

  def gpa(self):
    return self.qpoints/self.hours

def makeStudent(infoStr):
  # infoStr is a tab-separated line: name hours qpoints
  name, hours, qpoints = infoStr.split("\t")
  return Student(name, hours, qpoints)

def main():
  filename = input("Enter the name of the grade file: ")
  infile = open(filename, 'r')

  best = [makeStudent(infile.readline())]

  for line in infile:
    s = makeStudent(line)
    if s.gpa > best[0].gpa:
      best = [s]
    elif s.gpa = best[0].gpa:
      best.append(s)

  infile.close()

  if len(best) == 1:
    print("The best student is:", best[0].getName())
    print("Hours:", best[0].getHours())
    print("GPA:", best[0].gpa())
  else:
    print("The top students are:")
    for i in best:
      print("Name:", best[i].getName())
      print("Hours:", best[i].getHours())
      print("GPA:", best.gpa())

if __name__ == '__main__':
  main()
    