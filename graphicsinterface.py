from button import Button
from graphics import *
from dieview2 import DieView

class GraphicsInterface:
  def __init__(self):
    self.win = GraphWin("Dice Poker", 600, 400)
    self.win.setBackground("green3")
    banner = Text(Point(300, 30), "Python Poker Parlor")
    banner.setSize(24)
    banner.setFille("yellow2")
    banner.setStyle("bold")
    banner.draw(self.win)
    self.msg = Text(Point(300, 380), "Welcome to the Dice Table")
    self.msg.setSize(18)
    self.msg.draw(self.win)
    self.createDice(Point(300,100), 75)
    self.buttons = []
    self.addDiceButtons(Point(300, 170), 75, 30)
    b = Button(self.win, Point(300, 230), 400, 40, "Roll Dice")
    self.buttons.append(b)
    b = Button(self.win, Point(300, 280), 150, 40, "Score")
    self.buttons.append(b)
    b = Button(self.win, Point(570,375), 40, 30, "Quit")
    self.buttons.append(b)
    self.money = Text(Point(300,325), "$100")
    self.money.setSize(18)
    self.money.draw(self.win)

  def createDice(self, center, size):
    center.move(-3*size, 0)
    self.dice = []
    for i in range(5):
      view = DieView(self.win, center, size)
      self.dice.append(view)
      center.move(1.5*size, 0)

  def addDiceButtons(self, center, width, height):
    center.move(-3*width, 0)
    for i in range(1,6):
      label = "Die {0}".format(i)
      b = Button(self.win, center, width, height, label)
      self.buttons.append(b)
      center.move(1.5*width, 0)
      


