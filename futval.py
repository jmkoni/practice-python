# this program calculates future value
def main():
  print("This program calculates future value of an account given an initial balance and interest rate.")
  principal = eval(input("Please enter the principal balance: "))
  rate = eval(input("Please enter the yearly interest rate: "))
  periods = eval(input("Please enter the number of periods in a year: "))
  years = eval(input("Please enter the number of years: "))
  additional = eval(input("Please enter the additional amount you invest per year: "))
  for i in range(years * periods):
    principal = (principal * (1 + rate)) + (additional / periods)

  print("This is the amount you will have after", years, "years:", round(principal,2))

main()