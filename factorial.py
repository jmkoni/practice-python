def main():
  print("This program gets the factorial for any whole number.")
  n = eval(input("Please enter a whole number: "))
  fact = 1
  if n < 1:
    print("Please enter a value greater than zero")
    return
  if not isinstance(n, int):
    print("Please enter an integer")
    return
  for factor in range(n,1,-1):
    fact = fact * factor
  print("The factorial of", n, "is", fact)

main()